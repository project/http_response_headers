<?php

namespace Drupal\http_response_headers\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Markup;

/**
 * Provides a listing of Response Headers.
 */
class ResponseHeaderListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Header Name');
    $header['description'] = $this->t('Description');
    $header['value'] = $this->t('Value');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['description'] = Markup::create($entity->get('description'));
    $row['value'] = ($value = $entity->get('value')) ? mb_strimwidth($value, 0, 30, '...') : ' - ';
    return $row + parent::buildRow($entity);
  }

}

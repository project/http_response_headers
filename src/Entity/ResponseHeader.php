<?php

namespace Drupal\http_response_headers\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Response Header entity.
 *
 * @ConfigEntityType(
 *   id = "response_header",
 *   label = @Translation("Response Header"),
 *   handlers = {
 *     "list_builder" = "Drupal\http_response_headers\Controller\ResponseHeaderListBuilder",
 *     "form" = {
 *       "add" = "Drupal\http_response_headers\Form\ResponseHeaderForm",s
 *       "edit" = "Drupal\http_response_headers\Form\ResponseHeaderForm",
 *       "delete" = "Drupal\http_response_headers\Form\ResponseHeaderDeleteForm",
 *     }
 *   },
 *   config_prefix = "response_header",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "name",
 *     "value",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/response-headers/{response_header}",
 *     "delete-form" = "/admin/config/system/response-headers/{response_header}/delete",
 *   }
 * )
 */
class ResponseHeader extends ConfigEntityBase implements ResponseHeaderInterface {

  /**
   * The header ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human readable label.
   *
   * @var string
   */
  public $label;

  /**
   * The description.
   *
   * @var string
   */
  public $description;

  /**
   * The header name.
   *
   * @var string
   */
  public $name;

  /**
   * The header value.
   *
   * @var string
   */
  public $value;

}
